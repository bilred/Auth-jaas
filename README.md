This one is pluggable authentication module with all Java EE application / used JAAS -> Java Authentication and Authorization Service 

**Configuration project**

    1- install tomcat 7 or >
    2- add file jaas.config into folder /conf
    3- edit file /bin/catalina.bat

**instruction (1)**
 - download apache tomcat and unzip. 
 - setup JAVA_HOME and CATALINA_HOME.
  
**instruction (2)**
add this text below into file jaas.config

    AuthLoginJaas {
        ca.auth.jaas.AuthLoginModule required debug=true;
    };

note : 

    appName --> in file META-INF/context.xml
    appName {
        path --> package+className 
        REQUIRED
        debug=true;
    };

**instruction (3)**
add after the second ':okHome' into catalina.bat this ->

    echo Setup JASS...
    set JAVA_OPTS=%JAVA_OPTS% -Djava.security.auth.login.config=%CATALINA_HOME%/conf/jaas.config
    echo JAVA_OPTS %JAVA_OPTS%
  


**With intellij IDEA :** (optional)
- check 'Deploy applications configured in Tomcat instance'.
- add path application context.
